require "gitlab"

# 789333 - ID of my fork of CE
# 13083  - ID of CE

class GitlabStaleIssueBot
  bot = Gitlab.client(endpoint: 'https://gitlab.com/api/v3', private_token: ENV['GITLAB_API_PRIVATE_TOKEN'])

  GITLAB_CE = 13083

  puts bot.project(13083).name

  issue_count = bot.project(13083).open_issues_count

  puts "There are currently #{issue_count} open issues."

  pages = issue_count / 50

  puts "There are currently #{pages} pages of issues."

  # List all issues in the project.
  (1..pages).each do |num|
    issues = bot.issues(GITLAB_CE, page: num, per_page: 10)

    puts issues[0].id

    (1..issues.size - 1).each do |num|
      puts issues[num].labels.empty?
      puts issues[num].updated_at

      date = Date.parse(issues[num].updated_at)

      date2 = Date.today - date

      puts date2
    end
  end
end
