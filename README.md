# GitLab Stale Issue Bot

The intention is for this bot to browse all issues on the GitLab CE project and add the "Awaiting Feedback" label to issues that have become stale.

The criteria for adding the label is as-follows:

- No new responses for 3 months (not including system notes?)
- No milestone
- No assignee
- Possibly no label?

If those criterion are met, the bot applies the label and comments "This issue has become stale, is this still a problem?". If the issue recieves no response for 14 days, the bot will comment "Closing due to lack of response." and the issue will be closed.

This makes outdated issues less of a maintenance burden, and team members can focus on issues that actually effect current releases.

Resources:

- [gitlab gem documentation](http://www.rubydoc.info/gems/gitlab/3.4.0)
- [gitlab gem github repo](https://github.com/NARKOZ/gitlab)
- [Date class in Ruby](http://ruby-doc.org/stdlib-2.3.1/libdoc/date/rdoc/Date.html)
- [GitLab API Docs](http://doc.gitlab.com/ce/api/README.html)
